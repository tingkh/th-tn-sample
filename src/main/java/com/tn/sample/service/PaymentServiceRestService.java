/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tn.sample.service;

import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.jdbc.core.JdbcTemplate;

import com.tn.sample.impl.PaymentCdBSDAO;

/**
 * A JAX-RS resource for exposing REST endpoints for  manipulation
 */
@Path("/")
public class PaymentServiceRestService {
	@Resource (name="java:jboss/datasource/OracleDS")
	private DataSource jbpmDS;

    @Inject
    private PaymentCdBSDAO paymentCdBSDAO;

    JdbcTemplate jdbcTemplate = new JdbcTemplate();

    @POST
    @Path("updateKPTTPembayaran")
    // @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({"application/json"})
    public Response updateKPTTPembayaran(Map<String, Object> inputMap){
    	
		paymentCdBSDAO.enrichPaymentCdDetails(inputMap);
//    	kpttPembayaranDAOImpl = new KPTTPembayaranDAOImpl(); 
//    	kpttPembayaranDAOImpl.processKdPembayaran(inputMap);
    	
//    	synchronized (jdbcTemplate) {
//			if (this.jdbcTemplate.getDataSource()==null)
//			{
//				System.out.println("Initialize JDBC template");
//				jdbcTemplate.setDataSource(jbpmDS);
//			}
//		}
//    	
//		String sqlString = "select count(1) from task";
//		int count = this.jdbcTemplate.queryForObject(sqlString,Integer.class);
//
//		System.out.println("Count::" + count);
    	return Response.ok().build();
    }
    
    

   

}
