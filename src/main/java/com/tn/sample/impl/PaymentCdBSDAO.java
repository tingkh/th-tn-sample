package com.tn.sample.impl;

import java.util.Map;

import javax.ejb.Local;


@Local
public interface PaymentCdBSDAO {
	public void enrichPaymentCdDetails(Map<String, Object> inputMap);
}
