# BE-PAYMENT-SERVICE

## What is it?

This EAP service will be use for all insert / update to DB which is regarding to payment. 
This EAP module will also contains the payment logics



To install JDBC driver in EAP

mkdir -p [JBOSS_EAP_HOME_FOLDER_PARENT_FOLDER]/driver/oracle, or any location in the server
copy ojdbc8.jar to this folder 

Connect to jboss-cli
ip and port is depending on your EAP configuration

./jboss-cli.sh --connect controller=localhost:10090 

--adding oracle driver

module add --name=com.oracle --resources=/Users/tingkh/jboss/installers/driver/oracle/ojdbc8.jar --dependencies=javax.api,javax.transaction.api,sun.jdk

/subsystem=datasources/jdbc-driver=oracle:add(driver-name=oracle,driver-module-name=com.oracle,driver-xa-datasource-class-name=oracle.jdbc.xa.client.OracleXADataSource)

data-source add --name= H2HDS --jndi-name=java:jboss/datasources/H2HDS --driver-name=oracle --connection-url=jdbc:oracle:thin:@10.0.0.99:1521:labsdb --user-name=H2HLIVE --password=H2HLIVE --validate-on-match=true --background-validation=false --valid-connection-checker-class-name=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleValidConnectionChecker --exception-sorter-class-name=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleExceptionSorter --stale-connection-checker-class-name=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleStaleConnectionChecker

To test for connection 
/subsystem=datasources/data-source=H2HDS:test-connection-in-pool



--adding oracle driver

module add --name=org.jboss.teiid.client --resources=/Users/tingkh/jboss/installers/driver/oracle/ojdbc8.jar --dependencies=javax.api,javax.transaction.api

/subsystem=datasources/jdbc-driver=oracle:add(driver-name=oracle,driver-module-name=com.oracle,driver-xa-datasource-class-name=oracle.jdbc.xa.client.OracleXADataSource)

data-source add --name= H2HDS --jndi-name=java:jboss/datasources/H2HDS --driver-name=oracle --connection-url=jdbc:oracle:thin:@10.0.0.99:1521:labsdb --user-name=H2HLIVE --password=H2HLIVE --validate-on-match=true --background-validation=false --valid-connection-checker-class-name=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleValidConnectionChecker --exception-sorter-class-name=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleExceptionSorter --stale-connection-checker-class-name=org.jboss.jca.adapters.jdbc.extensions.oracle.OracleStaleConnectionChecker

To test for connection 
/subsystem=datasources/data-source=H2HDS:test-connection-in-pool


To add teeid driver

module add --name=org.jboss.teiid.client --resources=/Users/tingkh/jboss/installers/JDV/dataVirtualization/jdbc/teiid-8.12.5.redhat-8-jdbc.jar --dependencies=javax.api,javax.transaction.api,sun.jdk


module add --name=org.jboss.teiid.dialect --resources=/Users/tingkh/jboss/installers/JDV/dataVirtualization/jdbc/teiid-hibernate-dialect-8.12.5.redhat-8.jar --dependencies=javax.api,javax.transaction.api,sun.jdk

/subsystem=datasources/jdbc-driver=teiid:add(driver-name=teiid,driver-module-name=org.jboss.teiid.client,driver-class-name=org.teiid.jdbc.TeiidDriver,xa-datasource-class=org.teiid.jdbc.TeiidDataSource)

data-source add --name=P3VDATAMASTERDATA --jndi-name=java:jboss/datasources/P3VDATAMASTERDATA --driver-name=teiid --connection-url=jdbc:teiid:P3_VDATA_MASTERDATA@mm://10.0.130.35:31000 --user-name=teiidUser --password=admin123!@# --pool-prefill=false --flush-strategy=FailingConnectionOnly 


/subsystem=datasources/data-source=P3VDATAMASTERDATA:test-connection-in-pool
